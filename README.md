
EMcLAW is a Maxwell's equation solver including AMR, polarization, perfect metals and
divergence control.

The whole code can be used just pasting it inside AMReX (amrex-master/Tutorials/Amr)
as if it was an extra tutorial.

Brief user�s guide
To be able to use EMcLAW (https://bitbucket.org/pedro-velarde/emclaw) the first thing you need is
AMReX (https://amrex-codes.github.io/amrex/). This is a software framework library containing all the
functionality to write massively parallel, block-structured adaptive mesh refinement (AMR)
applications. AMReX can be downloaded from Github (https://github.com/AMReX-Codes/amrex) and
it has a lot of documentation for users (with tutorials) and developers. Although it is not a mandatory
step, a good way to start learning how to use EMcLAW is with the tutorials found in amrex-
master/Tutorials/Amr once you have downloaded AMReX. EMcLAW is designed to be pasted just here
and so it works like an extra tutorial (amrex-master/Tutorials/Amr/emclaw).

All the tests and tutorials, the ones in EMcLAW and the ones in AMReX, are easily compiled just
typing make. To run the executable just type ./executable_name inputs (you can use mpirun too). To
visualize the plots and to work with the data there are several alternatives like VisIt or yt (for python).

There is a visualization chapter in the documentation of AMReX.

Each EMcLAW test have the same files:

  -GNUmakefile: here you can choose the number of dimensions, MPI, OMP, profiling and debugging.

  -Make.package: the files to be compiled. This file should not be modified by an user. 

  -inputs: here an user can control when a simulation ends, the boundary conditions, the cfl, some
           refinement and regridding conditions and the plotfiles.

  -probin: you can change the automatic tagging depending on the energy density values or gradients.
  
  -DEFINES.H: here you can choose different automatic taggings depending on the permittivities, the
              polarization or if you want to permit forced tagging or not. You can here enable metallic materials,
              different polarizations and divergence control. You can also control if some variables are shown or not
              in the plots. When this file is changed you should type make clean and then make again to allow the
              changes to be done properly.

-init_src_pol.cpp: here the sources and the polarizations can be altered. With the polarizations you
                   should change the DEFINES.H file accordingly.

-Prob.f90: initial conditions are set here.

-ep_mu_(1/2/3)d.F90: the permittivities can be altered here. Some common geometries have short
                     ways to be written that can be found in DEFINES.H.

-metallic_material_(1/2/3)d.F90: to have perfect metals we need 2 subroutines and both should be
                                 changed to make our metals work. In DEFINES.H metallic materials have to be defined.

-force_tagging_(1/2/3)d.F90: you can put the mesh refinement wherever you want here.