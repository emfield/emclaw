// license_EMcLAW.txt
#include "sources.hpp"

namespace SOURCES {
//
//  TEMPORAL METHODS
//
//

//
// gaussian_src_time methods
//
gaussian_src_time::gaussian_src_time(Real f, Real fwidth, Real s, Real p)
{
	freq      = f;
	width     = 1.0 / fwidth;
	peak_time = width * s;
	cutoff    = width * s * 2;
	phase     = p;

  // this is to make lastSourceTime as small as possible
	while (exp(-cutoff*cutoff / (2*width*width)) < 1e-100)
	{
		cutoff *= 0.9;
	}
	cutoff = float(cutoff); // don't make cutoff sensitive to roundoff error
}

gaussian_src_time::gaussian_src_time(Real f, Real w, Real st, Real et, Real p)
{
	freq      = f;
	width     = w;
	peak_time = 0.5 * (st + et);
	cutoff    = (et - st) * 0.5;
	phase     = p;

	// this is to make lastSourceTime as small as possible
	while (exp(-cutoff*cutoff / (2*width*width)) < 1e-100)
	{
		cutoff *= 0.9;
	}
	cutoff = float(cutoff); // don't make cutoff sensitive to roundoff error
}

Real gaussian_src_time::dipole(Real time)
{
	Real tt = time - peak_time;

	if (float(fabs(tt)) > cutoff)
	{
		return 0.0;
	}
   else
   {
	Real amp = 1.0/(2*M_PI*freq);
	return exp(-tt*tt/(2*width*width))*sin(2*M_PI*freq*tt + phase)*amp;
   }
}

Real gaussian_src_time::current(Real time, Real dt)
{
	Real tt = time - peak_time;
   Real dip1 = dipole(tt);
   Real dip2 = dipole(tt+dt);

   Real J = (dip2-dip1)/dt; 

   return(J);
	// correction factor so that current amplitude (= d(dipole)/dt) is
	// ~ 1 near the peak of the Gaussian.
}


//
// continuous_src_time methods
//
Real continuous_src_time::dipole(Real time)
{
	if (time < start_time || time > end_time)
	{
		return 0.0;
	}
	// correction factor so that current amplitude (= d(dipole)/dt) is 1.
	Real amp = 1.0/(2*M_PI*freq);

	if (width == 0.0)
		return sin(2*M_PI*freq*time + phase)*amp;
	else
	{
		Real ts = (time - start_time) / width - slowness;
		Real te = (end_time - time) / width - slowness;

		return sin(2*M_PI*freq*time+phase)*amp*(1.0+tanh(ts))*(1.0+tanh(te))*0.25;
	}
}

Real continuous_src_time::current(Real time, Real dt)
{
   Real dip1 = dipole(time);
   Real dip2 = dipole(time+dt);

   Real J = (dip2-dip1)/dt; 

   return(J);
}


//
// trapezoidal_src_time methods
//
Real trapezoidal_src_time::dipole(Real time)
{
	if (time < start_time || time > end_time)
	{
		return 0.0;
	}
	// correction factor so that current amplitude (= d(dipole)/dt) is 1.
	Real amp = 1.0/(2*M_PI*freq);

	if (!width)
	{
		return sin(2*M_PI*freq*time + phase) * amp;
	}
	if (time <= start_time + width)
	{
		Real val = (time - start_time)/width;
		return sin(2*M_PI*freq*time + phase)*amp*val;
	}
	if (time >= end_time - width)
	{
		Real val = (end_time - time)/width;
		return sin(2*M_PI*freq*time + phase)*amp*val;
	}
	return sin(2*M_PI*freq*time + phase) * amp;
}

Real trapezoidal_src_time::current(Real time, Real dt)
{
   Real dip1 = dipole(time);
   Real dip2 = dipole(time+dt);

   Real J = (dip2-dip1)/dt; 

   return(J);
}

//
// n_pulse_src_time methods
//
Real n_pulse_src_time::current(Real t, Real)  {

	Real V_ret;

	if (t < t_init || t > t_end) {
		return 0.0;
	}

	        Real amp = 1.0/(2*M_PI*freq);

		V_ret = cos (2 * M_PI * freq * t + phase) * amp;

	return (V_ret);
}


}  //namespace
