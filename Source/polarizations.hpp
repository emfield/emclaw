// license_EMcLAW.txt
#ifndef POLARIZATIONS_H
#define POLARIZATIONS_H

#include <memory>

#include <AMReX_BoxArray.H>
#include <math.h>  
#include <AMReX_SPACE.H>
#include <AMReX_Amr.H>
#include <vector>     

using namespace amrex;

namespace POL {

enum Pol_type {
	kerr = 0, lorentz, HN
};

	//
	// class declaration
	//
	class C_polarization;
	class C_polarizations;
	class pol_time;


// Time-dependence of a polarization
class pol_time {
   public:
     typedef std::shared_ptr<pol_time> ptr_pol_t;

   public:
	//
	// Constructor
	//
	//
	pol_time(Real a = 0.0,
   Real o = 0.0,
   Real s = 0.0,
   Real g = 0.0,
   Real epi = 0.0,
   Real eps = 0.0,
   Real aHN = 0.0,
   Real bHN = 0.0,
   Real tau = 0.0
   ):
	alfa(a),
   omega_0(o),
   sigma(s),
   gamma_(g),
   ep_infHN(epi), 
   ep_sHN(eps), 
   alphaHN(aHN), 
   betaHN(bHN), 
   tauHN(tau)
   {}
	//
	// Destructor
	//
	virtual ~pol_time() {
	}
	//

//   private:
	//
   // kerr pol
	//
   Real alfa;
	//
   // lorentz pol
	//
	Real omega_0;
	//
	//
	//
	Real sigma;
	//
	//
	//
	Real gamma_;
   // HN pol
	//
	Real ep_infHN;
	//
	//
	//
	Real ep_sHN;
	//
	//
	//
	Real alphaHN;
	//
	//
	//
	Real betaHN;
	//
	//
	//
	Real tauHN;
};


class C_polarization {
   public:
	
	C_polarization(Pol_type Pt, const RealBox &rb,
	pol_time::ptr_pol_t & CV_pol_time) {

		my_Pol_type = Pt;
                my_pol_time = CV_pol_time;
                my_rb = rb;

	}

	virtual ~C_polarization() {
	}

	pol_time::ptr_pol_t my_pol_time;  // you can use one pointer in several polarizations

	Pol_type my_Pol_type;

	RealBox my_rb;  // Polarization RealBox 
};


class C_polarizations {
   public:

	C_polarizations() {
	}

	virtual ~C_polarizations() {
	}

	void add_pol(Pol_type Pt, const RealBox &rb,
	pol_time::ptr_pol_t & CV_pol_time) {

      my_Pols.push_back(C_polarization(Pt, rb, CV_pol_time));

	}

	int get_pol_type(int id) {
	     return (get(id).my_Pol_type);
	}

	RealBox get_pols_limits(int id) {
	     return (get(id).my_rb);
	}

	int size() {
		return (my_Pols.size());
	}

	Real get_kerr_param(int id) {
		return (get(id).my_pol_time->alfa);
	}

	Real get_omega_0(int id) {
	     return (get(id).my_pol_time->omega_0);
	}


	Real get_sigma(int id) {
	     return (get(id).my_pol_time->sigma);
	}


	Real get_gamma_(int id) {
	     return (get(id).my_pol_time->gamma_);
	}

	Real get_ep_infHN(int id) {
	     return (get(id).my_pol_time->ep_infHN);
	}

	Real get_ep_sHN(int id) {
	     return (get(id).my_pol_time->ep_sHN);
	}

	Real get_alphaHN(int id) {
	     return (get(id).my_pol_time->alphaHN);
	}

	Real get_betaHN(int id) {
	     return (get(id).my_pol_time->betaHN);
	}

	Real get_tauHN(int id) {
	     return (get(id).my_pol_time->tauHN);
	}

   C_polarization & get(int id)
   {
      BL_ASSERT( id>=0 && id<my_Pols.size() );
		return my_Pols[id];
   }
   const C_polarization & get(int id) const
   {
      BL_ASSERT( id>=0 && id<my_Pols.size() );
		return my_Pols[id];
   }

   public:
     typedef std::vector<C_polarization> list_pol_t;
   private:
     list_pol_t my_Pols;

};

} // POl

#endif //POLARIZATIONS_H
