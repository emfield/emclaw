! license_EMcLAW.txt
#include "DEFINES.H"

! ::: -----------------------------------------------------------
! ::: -----------------------------------------------------------
! ::: This routine will tag material borders 
! ::: 
! ::: INPUTS/OUTPUTS:
! ::: -----------------------------------------------------------
! ::: This routine will tag material borders 
! ::: 
! ::: INPUTS/OUTPUTS:
! ::: 
! ::: tag        <=  integer tag array
! ::: tag_lo,hi   => index extent of tag array
! ::: ep          => epsilon array
! ::: ep_lo,hi    => index extent of ep array
! ::: mu          => mu array
! ::: mu_lo,hi    => index extent of mu array
! ::: set         => integer value to tag cell for refinement
! ::: clear       => integer value to untag cell
! ::: lo,hi       => work region we are allowed to change
! ::: dx          => cell size
! ::: problo      => phys loc of lower left corner of prob domain
! ::: time        => problem evolution time
! ::: level       => refinement level of this array
! ::: -----------------------------------------------------------

subroutine materials_error(tag,tag_lo,tag_hi, &
                       ep,ep_lo,ep_hi, &
                       mu,mu_lo,mu_hi, &
                       set,clear,&
                       lo,hi,&
                       dx,problo,time,level) bind(C, name="materials_error")

  use tagging_params_module, only : emerr, emgrad, max_emerr_lev, max_emgrad_lev
  implicit none
  
  integer          :: lo(3),hi(3)
  integer          :: ep_lo(3),ep_hi(3)
  integer          :: mu_lo(3),mu_hi(3)
  integer          :: tag_lo(3),tag_hi(3)
  double precision :: ep(ep_lo(1):ep_hi(1), &
                            ep_lo(2):ep_hi(2), &
                            0:5)
  double precision :: mu(mu_lo(1):mu_hi(1), &
                            mu_lo(2):mu_hi(2), &
                            0:5)
  integer          :: tag(tag_lo(1):tag_hi(1),tag_lo(2):tag_hi(2),tag_lo(3):tag_hi(3))
  double precision :: problo(3),dx(3),time
  integer          :: level,set,clear

  double precision :: ax, ay
  integer          :: i, j, nc

  ! Tag on regions where ep or mu changes
        do nc = 0,2
        do    j = lo(2), hi(2) !1 ghost cell
           do i = lo(1), hi(1) !1 ghost cell
              ax = abs(ep(i-1,j,nc)-ep(i,j,nc))
              ax = max(ax, abs(ep(i,j,nc)-ep(i+1,j,nc)))
              ay = abs(ep(i,j-1,nc)-ep(i,j,nc))
              ay = max(ay, abs(ep(i,j,nc)-ep(i,j+1,nc)))
              if (max(ax,ay) .gt. 0.d0) then
                 tag(i,j,0) = set
              end if
           enddo
        enddo
        enddo
  
        do nc = 0,2
        do    j = lo(2), hi(2) !1 ghost cell
           do i = lo(1), hi(1) !1 ghost cell
              ax = abs(mu(i-1,j,nc)-mu(i,j,nc))
              ax = max(ax, abs(mu(i,j,nc)-mu(i+1,j,nc)))
              ay = abs(mu(i,j-1,nc)-mu(i,j,nc))
              ay = max(ay, abs(mu(i,j,nc)-mu(i,j+1,nc)))
              if (max(ax,ay) .gt. 0.d0) then
                 tag(i,j,0) = set
              end if
           enddo
        enddo
        enddo
  
end subroutine materials_error


! ::: -----------------------------------------------------------
! ::: This routine will tag sources 
! ::: 
! ::: INPUTS/OUTPUTS:
! ::: 
! ::: tag        <=  integer tag array
! ::: tag_lo,hi   => index extent of tag array
! ::: lo_source,hi_source => source limits
! ::: set         => integer value to tag cell for refinement
! ::: clear       => integer value to untag cell
! ::: lo,hi       => work region we are allowed to change
! ::: dx          => cell size
! ::: problo      => phys loc of lower left corner of prob domain
! ::: time        => problem evolution time
! ::: level       => refinement level of this array
! ::: -----------------------------------------------------------

subroutine sources_error(tag,tag_lo,tag_hi, &
                       lo_source,hi_source, &
                       set,clear,&
                       lo,hi,&
                       dx,problo,time,level) bind(C, name="sources_error")

  implicit none
  
  integer          :: lo(3),hi(3)
  integer          :: tag_lo(3),tag_hi(3)
  double precision :: lo_source(2), hi_source(2)
  integer          :: tag(tag_lo(1):tag_hi(1),tag_lo(2):tag_hi(2),tag_lo(3):tag_hi(3))
  double precision :: problo(3),dx(3),time
  integer          :: level,set,clear

  integer          :: i, j

! Sources
  double precision :: x,y

  ! Tag on regions where there are sources
  
  do    j = lo(2), hi(2)
   y = problo(2) + (dble(j)+0.5d0) * dx(2)
    if((y.le.hi_source(2)).and.(y.ge.lo_source(2))) then

     do i = lo(1), hi(1)
      x = problo(1) + (dble(i)+0.5d0) * dx(1)

       if((x.le.hi_source(1)).and.(x.ge.lo_source(1))) then
                 tag(i,j,0) = set
       endif

     end do
    endif
  end do

end subroutine sources_error


! ::: -----------------------------------------------------------
! ::: This routine will tag sources 
! ::: 
! ::: INPUTS/OUTPUTS:
! ::: 
! ::: tag        <=  integer tag array
! ::: tag_lo,hi   => index extent of tag array
! ::: lo_pol,hi_pol => polarization limits
! ::: set         => integer value to tag cell for refinement
! ::: clear       => integer value to untag cell
! ::: lo,hi       => work region we are allowed to change
! ::: dx          => cell size
! ::: problo      => phys loc of lower left corner of prob domain
! ::: time        => problem evolution time
! ::: level       => refinement level of this array
! ::: -----------------------------------------------------------

subroutine pol_error(tag,tag_lo,tag_hi, &
                       lo_pol,hi_pol, &
                       set,clear,&
                       lo,hi,&
                       dx,problo,time,level) bind(C, name="pol_error")

  implicit none
  
  integer          :: lo(3),hi(3)
  integer          :: tag_lo(3),tag_hi(3)
  double precision :: lo_pol(2), hi_pol(2)
  integer          :: tag(tag_lo(1):tag_hi(1),tag_lo(2):tag_hi(2),tag_lo(3):tag_hi(3))
  double precision :: problo(3),dx(3),time
  integer          :: level,set,clear

  integer          :: i, j, Phix, Phiy, Plox, Ploy

! Sources
  double precision :: x,y

  ! Tag on regions where there are polarization borders
  
  Phix = floor((hi_pol(1)-problo(1)-0.5d0*dx(1))/dx(1))
  Phiy = floor((hi_pol(2)-problo(2)-0.5d0*dx(2))/dx(2))
  Plox = ceiling((lo_pol(1)-problo(1)+0.5d0*dx(1))/dx(1))
  Ploy = ceiling((lo_pol(2)-problo(2)+0.5d0*dx(2))/dx(2))

  do    j = lo(2), hi(2)
!   y = problo(2) + (dble(j)+0.5d0) * dx(2)
!    if((y.le.hi_pol(2)).and.(y.ge.lo_pol(2))) then

     do i = lo(1), hi(1)
!      x = problo(1) + (dble(i)+0.5d0) * dx(1)
!       if((x.le.hi_pol(1)).and.(x.ge.lo_pol(1))) then

       if((j.le.Phiy).and.(j.ge.Ploy)) then
              if(i.eq.Plox) then
                 tag(Plox,j,0) = set
              endif
              if(i.eq.Phix) then
                 tag(Phix,j,0) = set
              endif
       endif

       if((i.le.Phix).and.(i.ge.Plox)) then
              if(j.eq.Ploy) then
                 tag(i,Ploy,0) = set
              endif
              if(j.eq.Phiy) then
                 tag(i,Phiy,0) = set
              endif
       endif

!       endif
     end do
!    endif
  end do

end subroutine pol_error
