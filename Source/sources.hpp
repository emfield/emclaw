// license_EMcLAW.txt
#ifndef SOURCES_H
#define SOURCES_H

#include <memory>

#include <AMReX_BoxArray.H>
#include <math.h> 
#include <AMReX_SPACE.H>
#include <AMReX_Amr.H>
#include <vector>      

using namespace amrex;

namespace SOURCES {

//enum Field_component {
//	Dx = 0, Dy, Dz, Bx, By, Bz
//};
enum Field_component {
	f1 = 0, f2, f3, f4, f5, f6
};

	//
	// class declaration
	//
	class C_source;
	class C_sources;
	class src_time;


// Time-dependence of a current source, intended to be overridden by
// subclasses.  current() and dipole() are be related by
// current = d(dipole)/dt (or rather, the finite-difference equivalent).
class src_time {
   public:
     typedef std::shared_ptr<src_time> ptr_src_t;

   public:
	//
	// Constructor
	//
	src_time() 
		:
      current_time(0.0),
		current_current(0.0)
   {}

	//
	// Destructor
	//
	virtual ~src_time() {
	}
	//

	virtual Real current(void) const {
		return current_current;
	}
	
   virtual Real current(Real time, Real dt) = 0; // pure virtual

	void update(Real time, Real dt) { //update values for current time
		if (time != current_time) {
			current_current = current(time, dt);
			current_time = time;
		}
	}

	virtual Real last_time() const {
		return 0.0;
	}

   private:
	//
	// Current time
	//
	Real current_time;
	//
	// Current current (J)
	//
	Real current_current;
};


class C_source {
   public:
	
	C_source(Field_component Fc, const RealBox &rb,
	Real Ampl, src_time::ptr_src_t & CV_src_time) {

		Amplitude = Ampl;
		my_Field_component = Fc;
      my_srce_time = CV_src_time;
      my_rb = rb;

	}

	virtual ~C_source() {
	}

	Real Amplitude;

	src_time::ptr_src_t my_srce_time;  // you can use one pointer in several sources

	Field_component my_Field_component;

	RealBox my_rb;  // Source RealBox 
};


class C_sources {
   public:

	C_sources() {
	}

	virtual ~C_sources() {
	}

	void add_source(Field_component Fc, const RealBox &rb,
	Real Ampl, src_time::ptr_src_t & CV_src_time) {

      my_Sources.push_back(C_source(Fc, rb, Ampl, CV_src_time));

	}

	Real get_current(int id) {
	     return (get(id).my_srce_time->current());
	}

	Real get_ampl(int id) {
	     return (get(id).Amplitude);
	}

	int get_field(int id) {
	     return (get(id).my_Field_component);
	}

	RealBox get_source_limits(int id) {
	     return (get(id).my_rb);
	}

	int size() {
		return (my_Sources.size());
	}

	void update(Real time_, Real dt_) {
		for (int idx = 0; idx < size(); idx++) {
			get(idx).my_srce_time->update(time_, dt_);
		}
	}

   C_source & get(int id)
   {
      BL_ASSERT( id>=0 && id<my_Sources.size() );
		return my_Sources[id];
   }
   const C_source & get(int id) const
   {
      BL_ASSERT( id>=0 && id<my_Sources.size() );
		return my_Sources[id];
   }

   public:
     typedef std::vector<C_source> list_src_t;
   private:
     list_src_t my_Sources;

};


// Gaussian-envelope source with given frequency, width, peak-time, cutoff
class gaussian_src_time
	:
		public src_time 
{
	public:
	//
	// Constructor
	//
	gaussian_src_time(Real f, Real fwidth, Real s = 5.0, Real phase = 0.0);
	gaussian_src_time(Real f, Real w, Real start_time, Real end_time, Real phase);
	//
	// Destructor
	//
	virtual ~gaussian_src_time() {}

   Real dipole(Real time);
	virtual Real current(Real time, Real dt);
	virtual Real last_time() const { return float(peak_time + cutoff); };
	//
	// Returns frequency
	//
	virtual Real frequency() const { return freq; }
	//
	// Sets frequency
	//
	virtual void set_frequency(Real f) { freq = f; }

	private:
	//
	// Frequency of the source
	//
	Real freq;
	//
	//
	//
	Real width;
	//
	//
	//
	Real peak_time;
	//
	//
	//
	Real cutoff;
	//
	//
	//
	Real phase;
}; //class gaussian_src_time


// Continuous (CW) source with (optional) slow turn-on and/or turn-off.
class continuous_src_time
	:
		public src_time
{
	public:
	//
	// Constructor
	//
	continuous_src_time
	(
		Real f,
		Real w = 0.0,
		Real st = 0.0,
		Real et = 1000.0,//infinity,
		Real s = 3.0,
		Real p = 0.0
	)
	:
		freq(f),
		width(w),
		start_time(float(st)),
		end_time(float(et)),
		slowness(s),
		phase(p)
	{}
	//
	// Destructor
	//
	virtual ~continuous_src_time() {}

   Real dipole(Real time);
	virtual Real current(Real time, Real dt);
	virtual Real last_time() const { return end_time; };
	//
	// Returns frequency
	//
	virtual Real frequency() const { return freq; }
	//
	// Sets frequency
	//
	virtual void set_frequency(Real f) { freq = f; }

	private:
	//
	// Frequency
	//
	Real freq;
	//
	//
	//
	Real width;
	//
	//
	//
	Real start_time;
	//
	//
	//
	Real end_time;
	//
	//
	//
	Real slowness;
	//
	// initial phase
	//
	Real phase;
}; //class continuous_src_time


// Trapezoidal source.
class trapezoidal_src_time
	:
		public src_time
{
	public:
	//
	// Constructor
	//
	trapezoidal_src_time
	(
		Real f,
		Real wd = 0.0,
		Real st = 0.0,
		Real et = 100.0,//infinity,
		Real p  = 0.0
	)
	:
		freq(f),
		width(wd),
		start_time(Real(st)),
		end_time(Real(et)),
		phase(p) 
	{
		if (width > (end_time - start_time)/2)
		{
			width = (end_time - start_time)/2;
		}
	}
	//
	// Destructor
	//
	virtual ~trapezoidal_src_time() {}

   Real dipole(Real time);
	virtual Real current(Real time, Real dt);
	virtual Real last_time() const { return end_time; };
	//
	// Returns frequency
	//
	virtual Real frequency() const { return freq; }
	//
	// Sets frequency
	//
	virtual void set_frequency(Real f) { freq = f; }

	private:
	//
	// Frequency
	//
	Real freq;
	//
	// time for going up or down
	//
	Real width;
	//
	// time when source turns on
	//
	Real start_time;
	//
	// time when source turns off
	//
	Real end_time;
	//
	// initial phase
	//
	Real phase;
}; //class trapezoidal_src_time


class n_pulse_src_time
   : 
      public src_time {
   public:
	//
	// Constructor
	//
	n_pulse_src_time(Real f, int V_nc, Real p, Real t0) 
       :
       src_time()
   {
		freq = f;
		nc = V_nc;
		phase = p;
		ww = 2 * M_PI * freq / (2 * nc);
		t_init = t0;
		t_end = nc / freq;
	}

	//
	// Destructor
	//
	virtual ~n_pulse_src_time() {
	}

	virtual Real current(Real time, Real dummy);

	virtual Real last_time() const {
		return (t_end);
	}

   private:
	//
	// Frequency of the source
	//
	Real freq;
	//
	// Width
	//
	Real ww;
	//
	// Number of cycles
	//
	int nc;
	//
	// Initial phase
	//
	Real phase;
	//
	// Start time
	//
	Real t_init;
	//
	// Stop time
	//
   Real t_end;
};


}// namespace  SOURCES
#endif //SOURCES_H
