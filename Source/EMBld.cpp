// license_AMReX.txt
#include <AMReX_LevelBld.H>
#include <EM.H>

using namespace amrex;

class EMBld
    :
    public LevelBld
{
    virtual void variableSetUp () override;
    virtual void variableCleanUp () override;
    virtual AmrLevel *operator() () override;
    virtual AmrLevel *operator() (Amr&            papa,
                                  int             lev,
                                  const Geometry& level_geom,
                                  const BoxArray& ba,
				  const DistributionMapping& dm,
                                  Real            time) override;
};

EMBld EM_bld;

LevelBld*
getLevelBld ()
{
    return &EM_bld;
}

void
EMBld::variableSetUp ()
{
    EM::variableSetUp();
}

void
EMBld::variableCleanUp ()
{
    EM::variableCleanUp();
}

AmrLevel*
EMBld::operator() ()
{
    return new EM;
}

AmrLevel*
EMBld::operator() (Amr&            papa,
		    int             lev,
		    const Geometry& level_geom,
		    const BoxArray& ba,
		    const DistributionMapping& dm,
		    Real            time)
{
    return new EM(papa, lev, level_geom, ba, dm, time);
}
