// license_EMcLAW.txt
#include <EM.H>
#include <EM_F.H>

#include "DEFINES.H"

using namespace amrex;

//
//Advance grids at this level in time.
//
Real
EM::advance (Real time,
              Real dt,
              int  iteration,
              int  ncycle)
{
    for (int k = 0; k < NUM_STATE_TYPE; k++) {
        state[k].allocOldData();
        state[k].swapTimeLevels(dt);
    }

    MultiFab& S_new = get_new_data(State_Type);

    const Real prev_time = state[State_Type].prevTime();
    const Real cur_time = state[State_Type].curTime();
    const Real ctr_time = 0.5*(prev_time + cur_time);

    const Real* dx = geom.CellSize();
    const Real* prob_lo = geom.ProbLo();

    //
    // Get pointers to Flux registers, or set pointer to zero if not there.
    //
    FluxRegister *fine    = 0;
    FluxRegister *current = 0;
    
    int finest_level = parent->finestLevel();

    if (do_reflux && level < finest_level) {
	fine = &getFluxReg(level+1);
	fine->setVal(0.0);
    }

    if (do_reflux && level > 0) {
	current = &getFluxReg(level);
    }

    MultiFab fluxes[BL_SPACEDIM];

    if (do_reflux)
    {
	for (int j = 0; j < BL_SPACEDIM; j++)
	{
	    BoxArray ba = S_new.boxArray();
	    ba.surroundingNodes(j);
	    fluxes[j].define(ba, dmap, NUM_STATE_U, 0);
	}
    }

    // State with ghost cells
    MultiFab Sborder(grids, dmap, NUM_STATE, NUM_GROW);
    FillPatch(*this, Sborder, NUM_GROW, time, State_Type, 0, NUM_STATE);

    // MF to hold the mac velocity
//    MultiFab Umac[BL_SPACEDIM];
//    for (int i = 0; i < BL_SPACEDIM; i++) {
//      BoxArray ba = S_new.boxArray();
//      ba.surroundingNodes(i);
//      Umac[i].define(ba, dmap, 1, iteration);
//    }


#ifdef _OPENMP
#pragma omp parallel
#endif
    {

	FArrayBox flux[BL_SPACEDIM], UL[BL_SPACEDIM], UR[BL_SPACEDIM];
   // epx,epy,epz,sqrtepx,sqrtepy,sqrtepz 
   // mux,muy,muz,sqrtmux,sqrtmuy,sqrtmuz 
	FArrayBox ep, mu;

	for (MFIter mfi(S_new, true); mfi.isValid(); ++mfi)
	{
	    const Box& bx = mfi.tilebox();

        FArrayBox& U_old = Sborder[mfi];
	FArrayBox& U_new = S_new[mfi];

   	ep.resize(amrex::grow(bx,NUM_GROW),6);
   	mu.resize(amrex::grow(bx,NUM_GROW),6);
	    // Allocate fabs
	    for (int i = 0; i < BL_SPACEDIM ; i++) {
	   	const Box& bxtmp = amrex::surroundingNodes(bx,i);
   		//flux[i].resize(bxtmp,NUM_STATE_U);
   		flux[i].resize(amrex::grow(bxtmp,NUM_GROW),NUM_STATE_U);
   		UL[i].resize(amrex::grow(bxtmp,NUM_GROW),NUM_STATE_U);
	   	UR[i].resize(amrex::grow(bxtmp,NUM_GROW),NUM_STATE_U);
         //To correctly have divergence control fluxes
   	 	flux[i].setVal(0.0);
         //To correctly do polarizations
   	 //	UL[i].setVal(0.0);
   	 //	UR[i].setVal(0.0);
	    }


       BL_PROFILE_VAR("total_advance",t_adv);

       BL_PROFILE_VAR("ep_mu",ep_mu);
       get_ep_mu(prob_lo,bx.loVect(), bx.hiVect(),
		   BL_TO_FORTRAN_3D(U_new),
			BL_TO_FORTRAN_3D(ep), 
			BL_TO_FORTRAN_3D(mu), 
		   dx,NUM_GROW,NUM_STATE,is_D_wave,time);
       BL_PROFILE_VAR_STOP(ep_mu);

//            for (int i = 0; i < BL_SPACEDIM ; i++) {
//                const Box& bxtmp = mfi.grownnodaltilebox(i, iteration);
//                Umac[i][mfi].copy(uface[i], bxtmp);
//            }

       BL_PROFILE_VAR("reconstruction",recons);
       reconstruction(bx.loVect(), bx.hiVect(),
			BL_TO_FORTRAN_3D(ep), 
		   BL_TO_FORTRAN_3D(mu), 
		   BL_TO_FORTRAN_3D(U_old), 
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(UR[0]), 
			  BL_TO_FORTRAN_3D(UR[1]), 
			  BL_TO_FORTRAN_3D(UR[2])), 
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(UL[0]), 
			  BL_TO_FORTRAN_3D(UL[1]), 
			  BL_TO_FORTRAN_3D(UL[2])), 
		   dx,dt,NUM_STATE,NUM_STATE_U,is_D_wave);
       BL_PROFILE_VAR_STOP(recons);

       f_sources.update(time,dt);
       for (int s = 0; s < f_sources.size();s++){
        const RealBox sl = f_sources.get_source_limits(s);
         sources_fields_face(prob_lo,bx.loVect(),bx.hiVect(),
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(UR[0]), 
			  BL_TO_FORTRAN_3D(UR[1]), 
			  BL_TO_FORTRAN_3D(UR[2])), 
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(UL[0]), 
			  BL_TO_FORTRAN_3D(UL[1]), 
			  BL_TO_FORTRAN_3D(UL[2])), 
         sl.lo(),sl.hi(),
         f_sources.get_ampl(s),
         f_sources.get_current(s),
         dx,dt,f_sources.get_field(s),NUM_STATE,NUM_STATE_U,NUM_GROW,time);//adding time
       }

       BL_PROFILE_VAR("integration",integ);
       integration(bx.loVect(), bx.hiVect(),
			BL_TO_FORTRAN_3D(ep), 
		   BL_TO_FORTRAN_3D(mu), 
		   BL_TO_FORTRAN_3D(U_old), 
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(UR[0]), 
			  BL_TO_FORTRAN_3D(UR[1]), 
			  BL_TO_FORTRAN_3D(UR[2])), 
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(UL[0]), 
			  BL_TO_FORTRAN_3D(UL[1]), 
			  BL_TO_FORTRAN_3D(UL[2])), 
		   dx, dt, NUM_STATE,NUM_STATE_U,is_D_wave);
       BL_PROFILE_VAR_STOP(integ);

       BL_PROFILE_VAR("transverse",trans);
       transverse(bx.loVect(), bx.hiVect(),
			BL_TO_FORTRAN_3D(ep), 
			BL_TO_FORTRAN_3D(mu), 
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(UR[0]), 
			  BL_TO_FORTRAN_3D(UR[1]), 
			  BL_TO_FORTRAN_3D(UR[2])), 
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(UL[0]), 
			  BL_TO_FORTRAN_3D(UL[1]), 
			  BL_TO_FORTRAN_3D(UL[2])), 
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(flux[0]), 
			  BL_TO_FORTRAN_3D(flux[1]), 
			  BL_TO_FORTRAN_3D(flux[2])), 
		   dx,dt,NUM_STATE,NUM_STATE_U,NUM_GROW, is_D_wave);
       BL_PROFILE_VAR_STOP(trans);

       BL_PROFILE_VAR("metal_mat",m_m);
#ifdef METALLIC_MATERIALS
       metallic_materials(prob_lo,bx.loVect(), bx.hiVect(),
		   BL_TO_FORTRAN_3D(U_old),
 		   AMREX_D_DECL(BL_TO_FORTRAN_3D(UR[0]), 
			  BL_TO_FORTRAN_3D(UR[1]), 
			  BL_TO_FORTRAN_3D(UR[2])), 
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(UL[0]), 
			  BL_TO_FORTRAN_3D(UL[1]), 
			  BL_TO_FORTRAN_3D(UL[2])), 
		   dx,NUM_GROW,NUM_STATE,NUM_STATE_U,is_D_wave);
#endif
       BL_PROFILE_VAR_STOP(m_m);

       BL_PROFILE_VAR("riemann",riemann);
       riemann(bx.loVect(), bx.hiVect(),
			BL_TO_FORTRAN_3D(ep), 
			BL_TO_FORTRAN_3D(mu), 
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(UR[0]), 
			  BL_TO_FORTRAN_3D(UR[1]), 
			  BL_TO_FORTRAN_3D(UR[2])), 
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(UL[0]), 
			  BL_TO_FORTRAN_3D(UL[1]), 
			  BL_TO_FORTRAN_3D(UL[2])), 
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(flux[0]), 
			  BL_TO_FORTRAN_3D(flux[1]), 
			  BL_TO_FORTRAN_3D(flux[2])), 
		   NUM_STATE,NUM_STATE_U,is_D_wave);
       BL_PROFILE_VAR_STOP(riemann);

       BL_PROFILE_VAR("advance",adv);
       advance_fields(bx.loVect(), bx.hiVect(),
		   BL_TO_FORTRAN_3D(U_old), 
		   BL_TO_FORTRAN_3D(U_new),
		   AMREX_D_DECL(BL_TO_FORTRAN_3D(flux[0]), 
			  BL_TO_FORTRAN_3D(flux[1]), 
			  BL_TO_FORTRAN_3D(flux[2])), 
		   dx, dt, NUM_STATE,NUM_STATE_U,is_D_wave);
       BL_PROFILE_VAR_STOP(adv);

       BL_PROFILE_VAR("sources",sources);
#ifdef DIV_CONTROL
       source_div(bx.loVect(), bx.hiVect(),
		   BL_TO_FORTRAN_3D(U_new),
		   dx, dt, NUM_STATE);
#endif

//       f_sources.update(time,dt);
//       for (int s = 0; s < f_sources.size();s++){
//       const RealBox sl = f_sources.get_source_limits(s);
//         sources_fields(prob_lo,bx.loVect(),bx.hiVect(),
//		   BL_TO_FORTRAN_N_3D(U_new,f_sources.get_field(s)),
//         sl.lo(),sl.hi(),
//         f_sources.get_ampl(s),
//         f_sources.get_current(s),dx,dt,time);
//       }
       f_sources.update(time,dt);
       for (int s = 0; s < f_sources.size();s++){
        const RealBox sl = f_sources.get_source_limits(s);
         sources_fields(prob_lo,bx.loVect(),bx.hiVect(),
		   BL_TO_FORTRAN_3D(U_new),
         sl.lo(),sl.hi(),
         f_sources.get_ampl(s),
         f_sources.get_current(s),f_sources.get_field(s),
	 NUM_STATE,dx,dt,time);
       }
       BL_PROFILE_VAR_STOP(sources);

#if N_POLARIZATIONS > 0
//Polarizations
       for (int s = 0; s < Polarizations.size();s++){
        const RealBox pl = Polarizations.get_pols_limits(s);

         if (Polarizations.get_pol_type(s) == 0){
         kerr_polarizations_field(prob_lo,bx.loVect(),bx.hiVect(),
		   BL_TO_FORTRAN_3D(U_new),
         pl.lo(),pl.hi(),
         Polarizations.get_kerr_param(s),
         dx,dt,s+1,NUM_STATE,is_D_wave);

         } else if (Polarizations.get_pol_type(s) == 1){
         lorentz_polarizations_field(prob_lo,bx.loVect(),bx.hiVect(),
		   BL_TO_FORTRAN_3D(U_new),
         pl.lo(),pl.hi(),
         Polarizations.get_omega_0(s),
         Polarizations.get_sigma(s),
         Polarizations.get_gamma_(s),
         dx,dt,s+1,NUM_STATE,is_D_wave,time);

         } else if (Polarizations.get_pol_type(s) == 2){
         HN_polarizations_field(prob_lo,bx.loVect(),bx.hiVect(),
		   BL_TO_FORTRAN_3D(U_old),
		   BL_TO_FORTRAN_3D(U_new),
         pl.lo(),pl.hi(),
         Polarizations.get_ep_infHN(s),
         Polarizations.get_ep_sHN(s),
         Polarizations.get_alphaHN(s),
         Polarizations.get_betaHN(s),
         Polarizations.get_tauHN(s),
         dx,dt,s+1,NUM_STATE,is_D_wave);
         }

       }

       BL_PROFILE_VAR("adv_pol",adv_pol);
       advance_pol(bx.loVect(), bx.hiVect(),
		   BL_TO_FORTRAN_3D(U_old), 
		   BL_TO_FORTRAN_3D(U_new),
		   dx, dt, NUM_STATE,is_D_wave);
       BL_PROFILE_VAR_STOP(adv_pol);

#endif

       BL_PROFILE_VAR("metal_fill",m_f);
#ifdef METALLIC_MATERIALS
         fill_metallic_materials(prob_lo,bx.loVect(), bx.hiVect(),
		   BL_TO_FORTRAN_3D(U_new),
		   dx,NUM_GROW,NUM_STATE,is_D_wave);
#endif
       BL_PROFILE_VAR_STOP(m_f);

       BL_PROFILE_VAR_STOP(t_adv);


	    if (do_reflux) {
		for (int i = 0; i < BL_SPACEDIM ; i++)
		    fluxes[i][mfi].copy(flux[i],mfi.nodaltilebox(i));	  
	    }
	}
    }

    if (do_reflux) {
	if (current) {
	    for (int i = 0; i < BL_SPACEDIM ; i++)
		current->FineAdd(fluxes[i],i,0,0,NUM_STATE_U,1.);
	}
	if (fine) {
	    for (int i = 0; i < BL_SPACEDIM ; i++)
		fine->CrseInit(fluxes[i],i,0,0,NUM_STATE_U,-1.);
	}
    }

#ifdef PARTICLES
    if (TracerPC) {
      TracerPC->AdvectWithUmac(Umac, level, dt);
    }
#endif

    return dt;
}
